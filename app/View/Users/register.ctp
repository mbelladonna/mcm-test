<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Register User'); ?></legend>
	<?php
		echo $this->Form->hidden('group_id', array('value' => 2));
		echo $this->Form->input('Email');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('FirstName');
		echo $this->Form->input('MiddleInitial');
		echo $this->Form->input('LastName');
		echo $this->Form->input('Suffix');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>